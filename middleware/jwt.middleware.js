const authUtilities = require('../util/auth.util');

module.exports = (req, res, next) => {
    console.log('In JWT middleware');
    const authHeader = req.headers.authorization;

    if (!authHeader) {
        return res.sendStatus(401);
    }

    const token = authHeader.split(' ')[1];

    const verifiedToken = authUtilities.verifyToken(token, 'access');
    if (!verifiedToken) {
        return res.sendStatus(403);
    }

    req.user = verifiedToken;
    next();
};
