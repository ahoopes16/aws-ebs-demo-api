const admin = (req, res, next) => {
    console.log('In Admin middleware');
    if (!req.user || req.user.role !== 'admin') {
        return res.sendStatus(403);
    }

    next();
};

const viewer = (req, res, next) => {
    console.log('In Viewer middleware');
    if (!req.user || req.user.role !== 'viewer') {
        return res.sendStatus(403);
    }

    next();
};

module.exports = {
    admin,
    viewer,
};
