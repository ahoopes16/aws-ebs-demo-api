# README

This repository is for a demo authentication API deployed to AWS Elastic Beanstalk.

## Built With

-   Node.js
-   Express.js
-   PostrgeSQL
-   AWS Elastic Beanstalk

## How to Run

### Local

1. Clone this repository
1. Add environment variables to your environment (reach out to [Alex](mailto:alexh@haystackdev.com) about these)
1. Navigate to this directory and run `yarn` or `npm i`
1. Run `yarn dev` or `npm run dev`
1. Import the included Postman environment and collection files

Behold! A working version of the API in your local environment.

I might look into containerizing this somehow.

### Deployed

1. Import the included Postman environment and collection files
1. Use the "Deployed" version of the environment

Behold! You can interact with the live version hosted in AWS (assuming it's still around, I probably won't keep it up for long)

## Resources

-   [AWS - Deploying Node.js Applications with Elastic Beanstalk](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create_deploy_nodejs.html)
-   [AWS - Adding an RDS Database Instance to your Node.js Application](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-nodejs.rds.html)
-   [Bitbucket - Deploying to AWS with Elastic Beanstalk Automatically](https://support.atlassian.com/bitbucket-cloud/docs/deploy-to-aws-with-elastic-beanstalk/)
