// Dependencies
const database = require('../database');

const createUser = userInformation => {
    const { email, password, role, firstName, lastName } = userInformation;
    return database.query(
        'INSERT INTO public.users (email, password, role, first_name, last_name) VALUES ($1::text, $2::text, $3::text, $4::text, $5::text)',
        [email, password, role, firstName, lastName]
    );
};

const findUserWithId = id => {
    return database.query('SELECT * FROM public.users WHERE id = $1', [id]);
};

const findUserWithEmail = email => {
    return database.query('SELECT * FROM public.users WHERE email = $1::text', [
        email,
    ]);
};

const deleteRefreshToken = token => {
    return database.query('DELETE FROM public.tokens WHERE token = $1::text', [
        token,
    ]);
};

const findRefreshToken = token => {
    return database.query(
        'SELECT * FROM public.tokens WHERE token = $1::text',
        [token]
    );
};

const saveRefreshTokenForUser = async (userId, newToken) => {
    const existingTokenResponse = await database.query(
        'SELECT * FROM public.tokens WHERE user_id = $1',
        [userId]
    );
    const existingToken = existingTokenResponse.rows[0];

    // If they already have a token entry, update the existing one
    if (existingToken) {
        return database.query(
            'UPDATE public.tokens SET token = $1::text WHERE id = $2',
            [newToken, existingToken.id]
        );
    }

    // Otherwise, create a new one
    return database.query(
        'INSERT INTO public.tokens (user_id, token) VALUES ($1, $2::text)',
        [userId, newToken]
    );
};

module.exports = {
    createUser,
    deleteRefreshToken,
    findRefreshToken,
    findUserWithEmail,
    findUserWithId,
    saveRefreshTokenForUser,
};
