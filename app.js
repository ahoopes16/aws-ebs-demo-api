// Dependencies
const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

// Routes
const healthRouter = require('./routes/health.router');
const authRouter = require('./routes/auth.router');
const viewerRouter = require('./routes/viewer.router');
const adminRouter = require('./routes/admin.router');

// Middleware
const jwt = require('./middleware/jwt.middleware');
const { admin, viewer } = require('./middleware/roles.middleware');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api/health', healthRouter);
app.use('/api/auth', authRouter);
app.use('/api/viewer', [jwt, viewer], viewerRouter);
app.use('/api/admin', [jwt, admin], adminRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500);
});

if (!module.parent) {
    const port = 3000;
    app.listen(port, () => {
        console.log(`Server listening on port ${port}!`);
    });
}

module.exports = app;
