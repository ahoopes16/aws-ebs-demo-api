// Dependencies
const authService = require('../services/auth.service');
const authUtilities = require('../util/auth.util');

// Controller Functions
const registerUser = async (req, res) => {
    const { email, password, role, firstName, lastName } = req.body;

    if (!email || !password || !role || !firstName || !lastName) {
        return res.status(400).send({
            message:
                'email, password, role, firstName, and lastName are required fields',
        });
    }

    const emailAlreadyExists = await authService.findUserWithEmail(email);
    if (emailAlreadyExists.rows.length > 0) {
        return res.status(400).send({
            message: `A user with the email "${email}" already exists`,
        });
    }

    await authService.createUser({
        email,
        password: authUtilities.hashPassword(password),
        role,
        firstName,
        lastName,
    });

    return res.sendStatus(201);
};

const loginUser = async (req, res) => {
    const { email, password } = req.body;

    if (!email || !password) {
        return res
            .status(400)
            .send({ message: 'email and password are required fields' });
    }

    const findUserResponse = await authService.findUserWithEmail(email);

    const existingUser = findUserResponse.rows[0];
    if (!existingUser) {
        return res
            .status(401)
            .send({ message: 'Email or password is incorrect' });
    }

    const passwordMatches = authUtilities.isSamePassword(
        password,
        existingUser.password
    );
    if (!passwordMatches) {
        return res
            .status(401)
            .send({ message: 'Email or password is incorrect' });
    }

    const tokenPayload = {
        email: existingUser.email,
        firstName: existingUser.first_name,
        lastName: existingUser.last_name,
        role: existingUser.role,
    };

    const accessToken = authUtilities.generateToken(tokenPayload, 'access');
    const refreshToken = authUtilities.generateToken(tokenPayload, 'refresh');

    await authService.saveRefreshTokenForUser(existingUser.id, refreshToken);

    return res
        .status(200)
        .send({ accessToken, refreshToken, message: 'Login successful!' });
};

const logoutUser = async (req, res, next) => {
    const { refreshToken } = req.body;

    if (!refreshToken) {
        return res
            .status(400)
            .send({ message: 'refreshToken is a required field' });
    }

    const deleteResponse = await authService.deleteRefreshToken(refreshToken);

    return res.status(200).send({ message: 'Logout successful!' });
};

const refreshToken = async (req, res) => {
    const { refreshToken } = req.body;

    if (!refreshToken) {
        return res
            .status(400)
            .send({ message: 'refreshToken is a required field' });
    }

    const existingTokenResponse = await authService.findRefreshToken(
        refreshToken
    );
    const existingToken = existingTokenResponse.rows[0];
    if (!existingToken) {
        return res.sendStatus(403);
    }

    // Important if we add expirations to refresh tokens
    const verifiedToken = authUtilities.verifyToken(refreshToken, 'refresh');
    if (!verifiedToken) {
        return res.sendStatus(403);
    }

    const tokenPayload = {
        email: verifiedToken.email,
        firstName: verifiedToken.firstName,
        lastName: verifiedToken.lastName,
        role: verifiedToken.role,
    };

    const accessToken = authUtilities.generateToken(tokenPayload, 'access');

    res.status(200).send({
        accessToken,
        message: 'Token refreshed successfully!',
    });
};

module.exports = {
    registerUser,
    loginUser,
    logoutUser,
    refreshToken,
};
