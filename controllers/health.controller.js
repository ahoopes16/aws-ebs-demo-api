const getHealth = (req, res, next) => {
    res.status(200).send({ message: 'API is alive and healthy!' });
};

module.exports = {
    getHealth,
};
