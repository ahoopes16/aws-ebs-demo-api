const getInformation = (req, res, next) => {
    res.status(200).send({
        message:
            'Congratulations, you are allowed to see this information because you have the "viewer" role!',
    });
};

module.exports = {
    getInformation,
};
