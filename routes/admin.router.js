// Dependencies
const express = require('express');

// Controllers
const adminController = require('../controllers/admin.controller');

// Utilities
const asyncHandler = require('../util/async-handler.util');

const router = express.Router();

router.get('/', asyncHandler(adminController.getInformation));

module.exports = router;
