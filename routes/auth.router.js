// Dependencies
const express = require('express');

// Controllers
const authController = require('../controllers/auth.controller');

// Utilities
const asyncHandler = require('../util/async-handler.util');

const router = express.Router();

router.post('/login', asyncHandler(authController.loginUser));
router.post('/logout', asyncHandler(authController.logoutUser));
router.post('/register', asyncHandler(authController.registerUser));
router.post('/token', asyncHandler(authController.refreshToken));

module.exports = router;
