// Dependencies
const express = require('express');

// Controllers
const viewerController = require('../controllers/viewer.controller');

// Utilities
const asyncHandler = require('../util/async-handler.util');

const router = express.Router();

router.get('/', asyncHandler(viewerController.getInformation));

module.exports = router;
