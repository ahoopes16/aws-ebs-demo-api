// Dependencies
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

// Password Functions
const hashPassword = password => bcrypt.hashSync(password, 10);

const isSamePassword = (password, hash) => bcrypt.compareSync(password, hash);

// Token Functions
const generateToken = (payload, tokenType = 'access') => {
    let token = '';

    switch (tokenType) {
        case 'access':
            token = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
                expiresIn: process.env.ACCESS_TOKEN_EXPIRE_TIME,
            });
            break;

        case 'refresh':
            token = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET);
            break;

        default:
            throw new Error(`Cannot generate token for type ${tokenType}`);
    }

    return token;
};

const verifyToken = (token, tokenType = 'access') => {
    let verified = false;
    switch (tokenType) {
        case 'access':
            verified = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
            break;

        case 'refresh':
            verified = jwt.verify(token, process.env.REFRESH_TOKEN_SECRET);
            break;

        default:
            throw new Error(`Cannot verify token for type ${tokenType}`);
    }
    return verified;
};

module.exports = {
    generateToken,
    hashPassword,
    isSamePassword,
    verifyToken,
};
